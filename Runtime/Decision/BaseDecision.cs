﻿using UnityEngine;

namespace AntiEntropy.StateMachine
{
  public abstract class BaseDecision : ScriptableObject
  {
    public abstract bool Decide(IStateController controller);
  }
}