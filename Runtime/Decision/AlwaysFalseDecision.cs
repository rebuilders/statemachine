﻿namespace AntiEntropy.StateMachine.Runtime
{
  public class AlwaysFalseDecision : BaseDecision
  {
    public override bool Decide(IStateController controller)
    {
      return false;
    }
  }
}
