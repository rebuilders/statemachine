﻿namespace AntiEntropy.StateMachine.Runtime
{
  public class AlwaysTrueDecision : BaseDecision
  {
    public override bool Decide(IStateController controller)
    {
      return true;
    }
  }
}