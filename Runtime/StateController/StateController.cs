﻿using System.Collections.Generic;

namespace AntiEntropy.StateMachine
{
  public class StateController : IStateController
  {
    private IState _currentState;
    private IState _startingState;
    private List<StateChange> _stateHistory;
    private int _maxHistorySize = 10;
    
    public StateController(State startingState)
    {
      _startingState = startingState;
      _currentState = startingState;
    }

    public void Tick()
    {
      _currentState.UpdateState(this);
    }

    public IState GetCurrentState()
    {
      return _currentState;
    }

    public List<StateChange> GetStateHistory()
    {
      return _stateHistory;
    }

    public void ChangeState(StateChange stateChange)
    {
      AddCurrentStateToHistory(stateChange.LastDecision, stateChange.TriggerValue);
      
      _currentState.ExitState(this);
      _currentState = stateChange.State;
      _currentState.EnterState(this);
    }

    private void AddCurrentStateToHistory(BaseDecision decision, bool transitionValue)
    {
      if (_stateHistory == null)
        _stateHistory = new List<StateChange>();
      
      var history = new StateChange {
        State = _currentState as State,
        LastDecision = decision,
        TriggerValue = transitionValue
      };
      _stateHistory.Add(history);

      while (_stateHistory.Count > _maxHistorySize)
        _stateHistory.RemoveAt(0);
    }

    public virtual void ResetStateController()
    {
      _currentState = _startingState;
      _stateHistory = new List<StateChange>();
    }
  }
}