﻿using System.Collections.Generic;

namespace AntiEntropy.StateMachine
{
  public interface IStateController
  {
    void Tick();
    IState GetCurrentState();
    List<StateChange> GetStateHistory();
    void ChangeState(StateChange stateChange);
    void ResetStateController();
  }
}