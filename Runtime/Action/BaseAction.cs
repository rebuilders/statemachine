﻿using UnityEngine;

namespace AntiEntropy.StateMachine
{
    public abstract class BaseAction : ScriptableObject
    {
        public abstract void Act(IStateController controller);
    }
}