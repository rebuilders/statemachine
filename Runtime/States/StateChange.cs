﻿using System;

namespace AntiEntropy.StateMachine
{
  [Serializable]
  public class StateChange
  {
    public State State;
    public BaseDecision LastDecision;
    public bool TriggerValue;
  }
}