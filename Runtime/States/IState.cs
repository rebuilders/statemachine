﻿namespace AntiEntropy.StateMachine
{
    public interface IState
    {
        void EnterState(IStateController controller);
        void ExitState(IStateController controller);
        void UpdateState(IStateController controller);
    }
}
