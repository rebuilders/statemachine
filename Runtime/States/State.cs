﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace AntiEntropy.StateMachine
{
  [CreateAssetMenu(menuName = "StateMachine/State")]
  public class State : ScriptableObject, IState
  {
    public List<BaseAction> Actions = new List<BaseAction>();

    [HideInInspector]public List<Transition> Transitions = new List<Transition>();

    private void Awake()
    {
      foreach (var action in Actions)
      {
        Assert.IsNotNull(action);
      }
      
      foreach (var transition in Transitions)
      {
        Assert.IsNotNull(transition);
        Assert.IsNotNull(transition.Decision);
        Assert.IsNotNull(transition.TrueState);
      }
    }

    public virtual void EnterState(IStateController controller)
    {
    }

    public virtual void ExitState(IStateController controller)
    {
    }

    public virtual void UpdateState(IStateController controller)
    {
      DoActions(controller);
      DoTransitions(controller);
    }

    private void DoActions(IStateController controller)
    {
      for (var i = 0; i < Actions.Count; i++)
      {
        Actions[i].Act(controller);
      }
    }

    private void DoTransitions(IStateController controller)
    {
      for (var i = 0; i < Transitions.Count; i++)
      {
        var currentTransition = Transitions[i];
        if (IsDecisionForTrueState(currentTransition, controller))
        {
          var stateChange = new StateChange{
            State = currentTransition.TrueState,
            LastDecision = currentTransition.Decision,
            TriggerValue = true
          };
          controller.ChangeState(stateChange);
          break;
        }

        if (currentTransition.FalseState != null)
        {
          var stateChange = new StateChange {
            State = currentTransition.FalseState,
            LastDecision = currentTransition.Decision,
            TriggerValue = false
          };
          controller.ChangeState(stateChange);
        }
      }
    }

    private bool IsDecisionForTrueState(Transition transition, IStateController controller)
    {
      return transition.Decision.Decide(controller);
    }
  }
}