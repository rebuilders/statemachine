﻿using System;

namespace AntiEntropy.StateMachine
{
    [Serializable]
    public class Transition
    {
        public BaseDecision Decision;
        public State TrueState;
        public State FalseState;
    }
}