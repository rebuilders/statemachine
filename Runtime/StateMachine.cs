﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace AntiEntropy.StateMachine
{
  public class StateMachine : MonoBehaviour
  {
    [Header("Runtime Info only - changes will not effect behaviour")]
    [SerializeField] private State _currentState;
    [SerializeField] private List<StateChange> _stateHistory;
    
    [Space]
    [Header("Actual State Machine Settings!")]
    [SerializeField] protected State _startingState;
    
#if UNITY_INCLUDE_TESTS
    public State CurrentState => _currentState;
    public List<StateChange> StateHistory => _stateHistory;
#endif
    
    protected IStateController _stateController;
    
    protected virtual void Awake()
    {
      Assert.IsNotNull(_startingState);
      _currentState = null;
      _stateHistory = new List<StateChange>();
      CreateStateController();
    }

    protected virtual void CreateStateController()
    {
      _stateController = new StateController(_startingState);
      UpdateStateInformation();
    }

    protected virtual void Update()
    {
      UpdateStateController();
    }

    private void UpdateStateController()
    {
      _stateController.Tick();
      UpdateStateInformation();
    }

    private void UpdateStateInformation()
    {
      _currentState = _stateController.GetCurrentState() as State;
      _stateHistory = _stateController.GetStateHistory();
    }
  }
}