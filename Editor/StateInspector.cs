﻿#region

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#endregion

namespace AntiEntropy.StateMachine.Editor
{
  [CustomEditor(typeof(State))]
  public class StateInspector : UnityEditor.Editor
  {
    private const string _arrowUpName = "arrow_up";
    private const string _arrowDownName = "arrow_down";
    private const string _removeItemName = "delete";

    private float _buttonHeight = 20;
    private float _buttonWidth = 23;
    
    private Texture2D _arrowUp;
    private Texture2D _arrowDown;
    private Texture2D _removeItem;

    private List<bool> _showDetails;
    private State _stateTarget;
    
    private void OnEnable()
    {
      _arrowUp = Resources.Load<Texture2D>(_arrowUpName);
      _arrowDown = Resources.Load<Texture2D>(_arrowDownName);
      _removeItem = Resources.Load<Texture2D>(_removeItemName);
      
      _stateTarget = (State)target;
      
      _showDetails = new List<bool>();
      for (int i = 0; i < _stateTarget.Transitions.Count; i++)
        _showDetails.Add(false);
    }

    public override void OnInspectorGUI()
    {
      DrawDefaultInspector();
      
      GUILayout.Space(20f); //2
      GUILayout.Label("Transitions", EditorStyles.boldLabel);

      using (new GUILayout.HorizontalScope())
      {
        GUILayout.Space(10f);
        using (new GUILayout.VerticalScope())
        {
          for (var i = 0; i < _stateTarget.Transitions.Count; i++)
          {
            var transition = _stateTarget.Transitions[i];
            
            var newIndex = ShowTransitionElement(transition, i);
            if (newIndex != i)
            {
              _stateTarget.Transitions.RemoveAt(i);
              _showDetails.RemoveAt(i);
              
              if (newIndex < 0)
              {
                break;
              }

              if (newIndex >= _stateTarget.Transitions.Count)
              {
                newIndex = _stateTarget.Transitions.Count;
              }

              _stateTarget.Transitions.Insert(newIndex, transition);
              _showDetails.Insert(newIndex, true);
              break;
            }
          }

          if (GUILayout.Button("Add Transition"))
          {
            _stateTarget.Transitions.Add(new Transition());
            _showDetails.Add(true);
          }
        }
      }

      _stateTarget.SetDirty();
      
      Repaint();
    }

    private int ShowTransitionElement(Transition transition, int position)
    {
      var name = "new Transition";
      if (transition.Decision != null)
      {
        name = transition.Decision.name;
      }

      _showDetails[position] = EditorGUILayout.Foldout(_showDetails[position], name);

      if (!_showDetails[position])
        return position;

      var newPosition = position;

      using (new GUILayout.HorizontalScope())
      {
        GUILayout.Space(20f);
        using (new GUILayout.VerticalScope())
        {
          EditorGUILayout.BeginHorizontal();

          transition.Decision = EditorGUILayout.ObjectField(transition.Decision, typeof(BaseDecision), false) as BaseDecision;

          if (GUILayout.Button(_arrowUp, GUILayout.Width(_buttonWidth), GUILayout.Height(_buttonHeight)))
          {
            newPosition = newPosition >= 1 ? position - 1 : 0;
          }

          if (GUILayout.Button(_arrowDown, GUILayout.Width(_buttonWidth), GUILayout.Height(_buttonHeight)))
          {
            newPosition = position + 1;
          }

          if (GUILayout.Button(_removeItem, GUILayout.Width(_buttonWidth), GUILayout.Height(_buttonHeight)))
          {
            newPosition = -1;
          }

          EditorGUILayout.EndHorizontal();

          transition.TrueState =
            EditorGUILayout.ObjectField("True State", transition.TrueState, typeof(State), false) as State;
          transition.FalseState =
            EditorGUILayout.ObjectField("False State", transition.FalseState, typeof(State),
              false) as State;
          GUILayout.Space(10f);
        }
      }

      return newPosition;
    }
  }
}