﻿using UnityEngine;

namespace AntiEntropy.StateMachine.CommonTests
{
  public class TestActionThree : BaseAction
  {
    public override void Act(IStateController controller)
    {
      Debug.Log("Action 3");
    }
  }
}