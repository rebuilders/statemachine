﻿using UnityEngine;

namespace AntiEntropy.StateMachine.CommonTests
{
  public class TestActionOne : BaseAction
  {
    public override void Act(IStateController controller)
    {
      Debug.Log("Action 1");
    }
  }
}