﻿using UnityEngine;

namespace AntiEntropy.StateMachine.CommonTests
{
  public class TestActionTwo : BaseAction
  {
    public override void Act(IStateController controller)
    {
      Debug.Log("Action 2");
    }
  }
}