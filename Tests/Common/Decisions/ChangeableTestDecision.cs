﻿namespace AntiEntropy.StateMachine.CommonTests
{
  public class ChangeableTestDecision : BaseDecision
  {
    public bool ReturnValue;
    public override bool Decide(IStateController controller)
    {
      return ReturnValue;
    }
  }
}