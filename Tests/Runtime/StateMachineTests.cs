﻿using System.Collections;
using AntiEntropy.StateMachine.CommonTests;
using AntiEntropy.StateMachine.Runtime;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace AntiEntropy.StateMachine.PlayModeTests
{
  public class StateMachineTests : MonoBehaviour
  {
    private const string _stateMachinePath = "StateMachine";
    private const string _stateOnePath = "States/TestState1";
    private const string _stateTwoPath = "States/TestState2";
    private const string _stateThreePath = "States/TestState3";
    private const string _alwaysTrueDecisionPath = "Decisions/Always True Decision";
    private const string _alwaysFalseDecisionPath = "Decisions/Always False Decision";
    private const string _changeableTestDecisionPath = "Decisions/Changeable Test Decision";

    private StateMachine _stateMachine;
    private State _stateOne;
    private State _stateTwo;
    private State _stateThree;
    private AlwaysTrueDecision _alwaysTrueDecision;
    private AlwaysFalseDecision _alwaysFalseDecision;
    private ChangeableTestDecision _changeableTestDecision;
    
    [SetUp]
    public void SetUp()
    {
      _stateOne = Resources.Load<State>(_stateOnePath);
      _stateTwo = Resources.Load<State>(_stateTwoPath);
      _stateThree = Resources.Load<State>(_stateThreePath);

      _alwaysTrueDecision = Resources.Load<AlwaysTrueDecision>(_alwaysTrueDecisionPath);
      _alwaysFalseDecision = Resources.Load<AlwaysFalseDecision>(_alwaysFalseDecisionPath);
      _changeableTestDecision = Resources.Load<ChangeableTestDecision>(_changeableTestDecisionPath);
      _changeableTestDecision.ReturnValue = false;
    }
    
    [TearDown]
    public void TearDown()
    {
    }
    
    [UnityTest]
    public IEnumerator StateMachineIsInstantiated_TicksAtEveryUpdate()
    {
      _stateMachine = Instantiate(Resources.Load<StateMachine>(_stateMachinePath));
      Assert.That(_stateMachine.CurrentState.Equals(_stateOne));
      yield return null;
      Assert.That(_stateMachine.CurrentState.Equals(_stateTwo));
      yield return null;
      Assert.That(_stateMachine.CurrentState.Equals(_stateTwo));
      _changeableTestDecision.ReturnValue = true;
      yield return null;
      Assert.That(_stateMachine.CurrentState.Equals(_stateThree));
      yield return null;
      Assert.That(_stateMachine.CurrentState.Equals(_stateOne));
    }
  }
}
