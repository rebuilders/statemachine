﻿using AntiEntropy.StateMachine.CommonTests;
using AntiEntropy.StateMachine.Runtime;
using NUnit.Framework;
using UnityEngine;

namespace AntiEntropy.StateMachine.EditorTests
{
  public class StateMachineTests
  {
    private const string _stateOnePath = "States/TestState1";
    private const string _stateTwoPath = "States/TestState2";
    private const string _stateThreePath = "States/TestState3";
    private const string _stateFourPath = "States/TestState4";
    private const string _alwaysTrueDecisionPath = "Decisions/Always True Decision";
    private const string _alwaysFalseDecisionPath = "Decisions/Always False Decision";
    private const string _changeableTestDecisionPath = "Decisions/Changeable Test Decision";

    private IStateController _stateController;
    private State _stateOne;
    private State _stateTwo;
    private State _stateThree;
    private State _stateFour;
    private AlwaysTrueDecision _alwaysTrueDecision;
    private AlwaysFalseDecision _alwaysFalseDecision;
    private ChangeableTestDecision _changeableTestDecision;
    
    [SetUp]
    public void Setup()
    {
      _stateOne = Resources.Load<State>(_stateOnePath);
      _stateTwo = Resources.Load<State>(_stateTwoPath);
      _stateThree = Resources.Load<State>(_stateThreePath);
      _stateFour = Resources.Load<State>(_stateFourPath);

      _alwaysTrueDecision = Resources.Load<AlwaysTrueDecision>(_alwaysTrueDecisionPath);
      _alwaysFalseDecision = Resources.Load<AlwaysFalseDecision>(_alwaysFalseDecisionPath);
      _changeableTestDecision = Resources.Load<ChangeableTestDecision>(_changeableTestDecisionPath);
      _changeableTestDecision.ReturnValue = false;
    }


    [Test]
    public void TestStateOneInitializedTicks_TransitionWillChangeToStateTwo()
    {
      _stateController = new StateController(_stateOne);
      
      Assert.That(_stateController.GetCurrentState().Equals(_stateOne));
      
      _stateController.Tick();
      
      Assert.That(_stateController.GetCurrentState().Equals(_stateTwo));
      Assert.That(_stateController.GetStateHistory()[0].LastDecision.Equals(_alwaysTrueDecision));
    }
    
    [Test]
    public void TestStateTwoInitializedTicks_StaysInSameState()
    {
      _stateController = new StateController(_stateTwo);
      
      Assert.That(_stateController.GetCurrentState().Equals(_stateTwo));
      
      _stateController.Tick();
      
      Assert.That(_stateController.GetCurrentState().Equals(_stateTwo));
    }
    
    [Test]
    public void TestStateFourTransitionReturnsFalse_MoveToFalseState()
    {
      _stateController = new StateController(_stateFour);
      
      Assert.That(_stateController.GetCurrentState().Equals(_stateFour));
      
      _stateController.Tick();
      
      Assert.That(_stateController.GetCurrentState().Equals(_stateThree));
    }
    
    [Test]
    public void TestStateTwoInitializedTicks_StaysInState_ChangeDecision_TransitionWillChangeToStateThree()
    {
      _stateController = new StateController(_stateTwo);
      
      Assert.That(_stateController.GetCurrentState().Equals(_stateTwo));
      
      _stateController.Tick();
      
      Assert.That(_stateController.GetCurrentState().Equals(_stateTwo));
      
      _changeableTestDecision.ReturnValue = true;
      
      _stateController.Tick();
      
      Assert.That(_stateController.GetCurrentState().Equals(_stateThree));
      Assert.That(_stateController.GetStateHistory()[0].LastDecision.Equals(_changeableTestDecision));
    }
    
    [Test]
    public void TestStateThreeInitializedTicks_TransitionWillChangeToStateOne()
    {
      _stateController = new StateController(_stateThree);
      
      Assert.That(_stateController.GetCurrentState().Equals(_stateThree));
      
      _stateController.Tick();
      
      Assert.That(_stateController.GetCurrentState().Equals(_stateOne));
      Assert.That(_stateController.GetStateHistory()[0].LastDecision.Equals(_alwaysTrueDecision));
    }


    [Test]
    public void StateMachineSwitchesStates_StateHistoryIsRecorded()
    {
      _changeableTestDecision.ReturnValue = true;

      _stateController = new StateController(_stateOne);
      
      for (int i = 0; i < 5; i++)
      {
        _stateController.Tick();
      }

      Assert.That(_stateController.GetStateHistory()[0].State.Equals(_stateOne));
      Assert.That(_stateController.GetStateHistory()[1].State.Equals(_stateTwo));
      Assert.That(_stateController.GetStateHistory()[2].State.Equals(_stateThree));
      Assert.That(_stateController.GetStateHistory()[3].State.Equals(_stateOne));
      Assert.That(_stateController.GetStateHistory()[4].State.Equals(_stateTwo));
    }
  }
}